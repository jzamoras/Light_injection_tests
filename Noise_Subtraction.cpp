//
//  Noise_Subtraction.cpp
//  
//
//  Created by Ricardo Zamora on 01/08/19.
//

#include <stdio.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TFile.h>
#include <TLegend.h>
#include <iostream>

TMultiGraph* subtract_noise(std::string pathToRaw, std::string pathToNoise);
std::string getFileName(std::string path);
void run_loop(std::string pathToRaw, std::string pathToNoise);

//This function receives a path to a csv file containing the raw data for a waveform and the noise data, and subtracts the noise from the raw data.
//Plots the data in a MultiGraph and returns the object for further manipulation
TMultiGraph* subtract_noise(std::string pathToRaw, std::string pathToNoise) {
    TFile* f1 = new TFile("Noise_Subtraction_Plots.root", "RECREATE");
    std:string fileName = getFileName(pathToRaw) + "_subtracted";
    
    TCanvas* c1 = new TCanvas(fileName.data(), fileName.data());
    TMultiGraph *mgr = new TMultiGraph();
    TGraph* gr1 = new TGraph(pathToRaw.data(), "%lg %lg", ",");
    TGraph* gr2 = new TGraph(pathToNoise.data(), "%lg %lg", ",");
    TGraph* gr3 = new TGraph();
    TLegend* tempLegend;
    
    int numberOfPoints;
    double xFull, yFull, xNoise, yNoise, ySubtracted;
    
    gr1->SetLineColor(kGreen);
    gr2->SetLineColor(kRed);
    gr3->SetLineColor(kBlue);
    
    numberOfPoints = gr1->GetN();
    
//    Assumes both raw and noise data have the exact same number of points
//    Hence the single counter/for-loop
    for (int i = 0; i < numberOfPoints; i++) {
        gr2->GetPoint(i, xNoise, yNoise);
        gr1->GetPoint(i, xFull, yFull);
        ySubtracted = yFull - yNoise;
        gr3->SetPoint(gr3->GetN(), xFull, ySubtracted);
    }
    
    std::string multiGraphTitle = fileName + "; Time [ns]; Voltage [V]";
    mgr->SetName(fileName.data());
    mgr->SetTitle(multiGraphTitle.data());
    gr1->SetTitle("Full Waveform");
    gr1->SetName("Full Waveform");
    gr2->SetTitle("Noise");
    gr2->SetName("Noise");
    gr3->SetTitle("Subtracted");
    gr3->SetName("Subtracted");
    mgr->Add(gr1);
    mgr->Add(gr2);
    mgr->Add(gr3);
    
    c1->cd();
    //    mgr->Draw("a fb l3d"); //3D Plot
    mgr->Draw("a");
    tempLegend = c1->BuildLegend();
    
    c1->Write();
    mgr->Write();
    gr1->Write();
    gr2->Write();
    gr3->Write();
//    tempLegend->Write();
    
    f1->Close();
    return mgr;
}

//This function finds the substring to be used for the filename of the saved plots
//Dirty and hardcoded

std::string getFileName(std::string path){
    std::string fileName;
    int startingIndex, endingIndex;
    
    //Hardcoded to the naming scheme currently being used
    startingIndex = path.rfind("/") + 1;
    endingIndex = path.find("_raw") == std::string::npos ? path.find("_noise") : path.find("_raw");
    
    fileName = path.substr(startingIndex, endingIndex-startingIndex);
    
    return fileName;
}
