//
//  Waveform_to_FFT.cpp
//  
//
//  Created by Ricardo Zamora on 01/08/19.
//

#include <stdio.h>
#include <TCanvas.h>
#include <TGraph.h>
#include "TH1D.h"
#include "TVirtualFFT.h"

int run();
int compute_fft(std::string path);

void run(){
    compute_fft("/Users/ricardo/Downloads/CERN/Measurements/20190731/OPV310_noise_wf_no_bias.csv");
}

int compute_fft(std::string path) {
    TCanvas* c1 = new TCanvas("c1", "OPV310");
    TCanvas* c2 = new TCanvas("c3", "OPV310 Histo");
    TCanvas* c3 = new TCanvas("c5", "OPV310 FFT");
    
    TGraph* gr1 = new TGraph(path.data(), "%lg %lg", ",");
    TH1D *gr1Histo = new TH1D("gr1Histo", "gr1Histo", 1000, -10.0E-9, 50.0E-9);
    TH1 *fft1 = 0;
    TFile* f1 = new TFile("Waveform_to_FFT.root", "RECREATE");
    
    int i, numPoints;
    double x, y, startTime, endTime, length;
    
    numPoints = gr1->GetN();
    for (i = 0; i < numPoints; i++) {
        gr1->GetPoint(i, x, y);
        gr1Histo->Fill(x, y);
    }
    
    fft1 = gr1Histo->FFT(fft1, "MAG R2C EX");
    fft1->SetTitle("Magnitude of the 1st transform (OPV310)");
    
    c1->cd();
    gr1->Draw();
    c2->cd();
    gr1Histo->Draw();
    c3->cd();
    fft1->Draw();
    
    gr1->Write();
    fft1->Write();
    f1->Close();
    
    return 0;
}
