//
//  Noise_Subtraction.h
//  
//
//  Created by Ricardo Zamora on 07/08/19.
//

TMultiGraph* subtract_noise(std::string pathToRaw, std::string pathToNoise);
std::string getFileName(std::string path);

#ifndef Noise_Subtraction_h
#define Noise_Subtraction_h

#endif /* Noise_Subtraction_h */
